// Fill out your copyright notice in the Description page of Project Settings.


#include "Grabber.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

std::pair<bool, FHitResult> UGrabber::GetHitComponent() const
{
	FVector Start = GetComponentLocation();
	FVector End = Start + GetForwardVector() * MaximumGrabbableDistance;

	FCollisionShape CollisionShpere = FCollisionShape::MakeSphere(CollisionShpereRadius);
	
	FHitResult Result;
	bool HasHit = GetWorld()->SweepSingleByChannel(
		Result,
		Start,
		End,
		FQuat::Identity,
		ECC_GameTraceChannel2,
		CollisionShpere
	);

	return { HasHit, Result };
}

UPhysicsHandleComponent* UGrabber::GetPhysicsHandle() const
{
	auto result = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (!result)
		UE_LOG(LogTemp, Error, TEXT("Couldn't find a physics handler."));
	return result;
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (auto PhysicsHandle = GetPhysicsHandle()) {
		if (!PhysicsHandle->GetGrabbedComponent()) return;
		FVector Target = GetComponentLocation() + GetForwardVector() * HoldingDistance;
		PhysicsHandle->SetTargetLocationAndRotation(Target, GetComponentRotation());
	}
}

void UGrabber::Grab()
{
	auto [HasHit, Result] = GetHitComponent();
	if (!HasHit) return;

	if (auto PhysicsHandle = GetPhysicsHandle()) {
		auto Component = Result.GetComponent();
		Component->WakeAllRigidBodies();
		Component->SetSimulatePhysics(true);
		AActor* Actor = Result.GetActor();
		Actor->Tags.Add("Grabbed");
		Actor->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		PhysicsHandle->GrabComponentAtLocationWithRotation(
			Component,
			NAME_None, 
			Result.ImpactPoint, 
			GetComponentRotation()
		);
	}
}

void UGrabber::Release()
{
	if (auto PhysicsHandle = GetPhysicsHandle()) {
		auto Component = PhysicsHandle->GetGrabbedComponent();
		if (!Component) return;
		Component->GetOwner()->Tags.Remove("Grabbed");
		PhysicsHandle->ReleaseComponent();
		UE_LOG(LogTemp, Display, TEXT("Relased"));
	}
}

